#!/bin/sh
zypper in python311 python311-pip libicu
rpm --import https://packages.microsoft.com/keys/microsoft.asc
wget https://packages.microsoft.com/config/opensuse/15/prod.repo
mv prod.repo /etc/zypp/repos.d/microsoft-prod.repo
chown root:root /etc/zypp/repos.d/microsoft-prod.repo

